Para poder ejecutar los programas del proyecto.

1. El script "ibex_data_collect.sh" es el encargado de automatizar la captura de información 
y cuando termina el día esta se sube al HDFS. Por lo tanto es el primer script a ejecutar en caso de no tener
datos.


2. Por otro lado hay un .sh por cada ejercicio, los cuales ejecutan los programas MapReduce en python
que se encuentran en la carpeta "mapReducers". Estos scripts hacen un filtrado previo de los csv necesarios
para obtener el resultado adecuado en función de cada ejercicio.

3. Para algunos ejercicios se crea un csv temporal, este se almacena en la carpeta hdsfFiles y se sube al HDFS desde
donde se ejecuta, después de la ejecución este archivo se elimina automáticamente

####################################
###### Listado de launchers ########
####################################

1. Ejercicio 1 -> ./launch_1_listadoSemanal.sh --> Tiene en cuenta desde el día actual todos los días de la semana transcurridos (hasta el lunes)
2. Ejercicio 2 -> ./launch_2_listadoMensual.sh --> Desde el día de ejecución tiene en cuenta todo el mes
1. Ejercicio 3 -> ./launch_3_infoAccion.sh --> Requiere String:NombreDeEmpresa, YYYY/MM/DD:FechaInicio y YYYY/MM/DD:FechaFin
1. Ejercicio 4 -> ./launch_4_infoUltimosValores.sh --> Requiere String:NombreDeEmpresa
1. Ejercicio 5 -> ./launch_5_infoMejoresAcciones.sh --> Tiene en cuenta el ultimo valor cotizado cada día durante la ultima semana y mes
1. Ejercicio 6 -> ./launch_6_infoPeoresAcciones.sh --> Tiene en cuenta el ultimo valor cotizado cada día durante la ultima semana y mes

Ejemplo de ejecución:

# Ejercicio 3
./launch_3_infoAccion.sh INDITEX 2022/04/24 2022/04/26

# Ejercicio 1
./launch_1_listadoSemanal.sh
