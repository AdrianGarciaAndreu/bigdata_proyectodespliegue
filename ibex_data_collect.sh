#!/bin/bash


#Fecha actual
currentDate=$(date '+%d %m %Y %H %M')

day=$(echo $currentDate | awk '{print $1}')
month=$(echo $currentDate | awk '{print $2}')
year=$(echo $currentDate | awk '{print $3}')

hour=$(echo $currentDate | awk '{print $4}')
minute=$(echo $currentDate | awk '{print $5}')

#Archivo de salida
outputFile="ibex_"$year"_"$month"_"$day".csv"


enabler=0
((minsToHour=60-minute))
((minsToHour=minsToHour*60)) # Tiempo aproximado de espera hasta que habra el IBEX35 en caso de que sea pronto

	while [ $enabler -eq 0 ]
	do	
		if ([ $hour -ge 18 ] && [ $minute -gt 30 ]) || [ $hour -gt 19 ] #HORA DE CIERRE
		then
			echo "End for today"
			echo "Uploading file to HDFS..."
			#Subida del archivo al HDFS
			hdfs dfs -mkdir -p "ibex/"$year"/"$month
			hdfs dfs -put -f $outputFile "ibex/"$year"/"$month"/"

			#rm $outputFile #Vacia el disco local

			((enabler=1))

		else  # Si aun es pronto
			if ([ $hour -ge 9 ] && [ $minute -gt 30 ]) || [ $hour -gt 10 ] # HORA DE APERTURA
			then	
				#Actualuzacion de tiempo
				currentDate=$(date '+%d %m %Y %H %M')
				hour=$(echo $currentDate | awk '{print $4}')
				minute=$(echo $currentDate | awk '{print $5}')

				
				echo $(date)" Scraper launched"
					
				#Procesado de la informacion
				python scraper.py >> $outputFile
				
				echo $(date)" Hour "$hour "recorded, waiting 3600 seconds"


				sleep 3600 # Espero 1 Hora
			else

				((timeToWait=minsToHour)) 
				((minsToHour=3600))

				echo $(date)" Aun no es la hora, esperando "$timeToWait" seconds..."
				sleep $timeToWait
			fi

		fi 
	done # Fin de bucle



echo "Finish"
