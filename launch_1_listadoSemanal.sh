#! /bin/bash

#Fecha actual
currentDate=$(date '+%d %m %Y %u')

daysOfWeek=$(echo $currentDate | awk '{print $4}')
((daysOfWeek=daysOfWeek-1))

counter=0
listOfFiles=()

for i in `seq $daysOfWeek -1 0`
do
  currentDayOfWeek=$(date  '+%d %m %Y' --date "$i days ago")
  
  day=$(echo $currentDayOfWeek | awk '{print $1}')
  month=$(echo $currentDayOfWeek | awk '{print $2}')
  year=$(echo $currentDayOfWeek | awk '{print $3}')
    
  hdfsPathPrefix="hdfs:///user/alumno/ibex/"$year"/"$month"/"

    hdfs dfs -test -f "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv"
    if [ $? -eq 0 ]
    then
        echo "Found ibex_"$year"_"$month"_"$day".csv"
        listOfFiles+=($hdfsPathPrefix"ibex_"$year"_"$month"_"$day".csv")
    else
        echo "."
    fi
done


listForLaunch=""
for data in "${listOfFiles[@]}"
do
    listForLaunch+="$data "
done

python mapReducers/listadoSemanal.py -r hadoop $listForLaunch --output-dir "ibex/"$year"/"$month"/results/WeeklyInfo/"
hdfs dfs -cat "ibex/"$year"/"$month"/results/WeeklyInfo/part*"
