#! /bin/bash


if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]
then
    echo "Introduzca String:[ACCION] yyy/mm/dd:[fecha inicio] y yyyy/mm/dd:[fecha inicio]"
else

    #Fecha actual
    fInicio=$(date -d $2 '+%d %m %Y')
    fFin=$(date -d $3 '+%d %m %Y')

    
    diff_days=$(( ($(date -d $3 +%s) - $(date -d $2 +%s) )/(60*60*24) ))

    for i in `seq 0 1 $diff_days`
    do
    currentDayOfWeek=$(date -d "$3 $i days ago" '+%d %m %Y')

    day=$(echo $currentDayOfWeek | awk '{print $1}')
    month=$(echo $currentDayOfWeek | awk '{print $2}')
    year=$(echo $currentDayOfWeek | awk '{print $3}')

    hdfsPathPrefix="hdfs:///user/alumno/ibex/"$year"/"$month"/"        
        
        hdfs dfs -test -f "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv"
        if [ $? -eq 0 ]
        then
            echo "Found ibex_"$year"_"$month"_"$day".csv"
            listOfFiles+=($hdfsPathPrefix"ibex_"$year"_"$month"_"$day".csv")
        else
            echo "."
        fi
    done


    listForLaunch=""
    # Launch python
    for data in "${listOfFiles[@]}"
    do
        listForLaunch+="$data "
    done

    echo $listForLaunch
    python mapReducers/infoAccion.py -r hadoop $listForLaunch --output-dir "ibex/"$year"/"$month"/results/stockInfo/" --accion=$1
    hdfs dfs -cat "ibex/"$year"/"$month"/results/stockInfo/part*"

fi


