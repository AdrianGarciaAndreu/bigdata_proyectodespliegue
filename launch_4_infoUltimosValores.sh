#! /bin/bash


if [ -z "$1" ]
then
    echo "Introduzca String:[ACCION]"
else


#Fecha actual
currentDate=$(date '+%d %m %Y %u')

dayOfMonth=$(echo $currentDate | awk '{print $1}')
((dayOfMonth=dayOfMonth-1))

dayOfWeek=$(echo $currentDate | awk '{print $4}')
((counter=dayOfWeek))

listOfFiles=()

for i in `seq 0 1 $dayOfMonth`
do
  currentDayOfWeek=$(date  '+%d %m %Y' --date "$i days ago")

  
  day=$(echo $currentDayOfWeek | awk '{print $1}')
  month=$(echo $currentDayOfWeek | awk '{print $2}')
  year=$(echo $currentDayOfWeek | awk '{print $3}')
    

  hdfsPathPrefix="hdfs:///user/alumno/ibex/"$year"/"$month"/"      

  fieldType=$(echo "MENSUAL")
  today=0

  if [ $counter -lt 0 ] 
  then
    ((today=0))
    fieldType=$(echo "MENSUAL")
  else 
    if [ $counter -eq $dayOfWeek ]
    then
        ((today=1))
    else 
        ((today=0))
    fi

    fieldType=$(echo "SEMANAL")
  fi

    hdfs dfs -test -f "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv"
    if [ $? -eq 0 ]
    then
        
        echo "File found for "$year"/"$month"/"$day
        hdfs dfs -cat "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv" | awk -F ',' -v fieldType=$fieldType -v hoy=$today '{print $0 ","fieldType"," hoy}' >> "hdfsFiles/ibex_"$year"_"$month"_temp.csv"
    else
        echo "."
    fi
    
    ((counter=counter-1))

done

#python infoUltimosValores.py "ibex_"$year"_"$month"_temp.csv" --accion=$1
#rm "ibex_"$year"_"$month"_temp.csv"

hdfs dfs -mkdir -p "ibex/"$year"/"$month"/temp/"
hdfs dfs -put -f "hdfsFiles/ibex_"$year"_"$month"_temp.csv" "ibex/"$year"/"$month"/temp/ibex_"$year"_"$month"_temp.csv"
rm "hdfsFiles/ibex_"$year"_"$month"_temp.csv"

python mapReducers/infoUltimosValores.py -r hadoop $hdfsPathPrefix"temp/ibex_"$year"_"$month"_temp.csv" --output-dir "ibex/"$year"/"$month"/results/lastValues/" --accion=$1
hdfs dfs -cat "ibex/"$year"/"$month"/results/lastValues/part*"

fi


