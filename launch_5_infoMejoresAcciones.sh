#! /bin/bash

#Fecha actual
currentDate=$(date '+%d %m %Y %u')

dayOfMonth=$(echo $currentDate | awk '{print $1}')
((dayOfMonth=dayOfMonth-1))

dayOfWeek=$(echo $currentDate | awk '{print $4}')
((counter=dayOfWeek))

listOfFiles=()

for i in `seq $dayOfMonth -1 0`
do
  currentDayOfWeek=$(date  '+%d %m %Y' --date "$i days ago")

  
  day=$(echo $currentDayOfWeek | awk '{print $1}')
  month=$(echo $currentDayOfWeek | awk '{print $2}')
  year=$(echo $currentDayOfWeek | awk '{print $3}')

  
  fieldType=$(echo "MENSUAL")

  if [ $i -lt $dayOfWeek ] 
  then
    fieldType=$(echo "SEMANAL")
  else 
    fieldType=$(echo "MENSUAL")
  fi

    hdfs dfs -test -f "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv"
    if [ $? -eq 0 ]
    then
        echo "File found for "$year"/"$month"/"$day
        hdfs dfs -cat "ibex/"$year"/"$month"/ibex_"$year"_"$month"_"$day".csv" | awk -F ',' -v fieldType=$fieldType '{print $0 ","fieldType}' >> "hdfsFiles/ibex_"$year"_"$month"_temp.csv"
    else
        echo "."
    fi
    
    ((counter=counter-1))

done


hdfs dfs -mkdir -p "ibex/"$year"/"$month"/temp/"
hdfs dfs -put -f "hdfsFiles/ibex_"$year"_"$month"_temp.csv" "ibex/"$year"/"$month"/temp/ibex_"$year"_"$month"_temp.csv"
rm "hdfsFiles/ibex_"$year"_"$month"_temp.csv"


python mapReducers/infoMejoresAcciones.py -r hadoop "hdfs:///user/alumno/ibex/"$year"/"$month"/temp/ibex_"$year"_"$month"_temp.csv" --output-dir "ibex/"$year"/"$month"/results/bestResults/"
hdfs dfs -cat "ibex/"$year"/"$month"/results/bestResults/part*"



