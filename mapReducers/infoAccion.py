from calendar import c
from ctypes import sizeof
from logging.config import dictConfig
from datetime import datetime

from mrjob.job import MRJob
from mrjob.step import MRStep


import collections
import unicodedata

class MRfirstJoin(MRJob):

    def configure_args(self):
        super(MRfirstJoin, self).configure_args()
        self.add_passthru_arg('--accion', default='ACCIONA', help='Introduce la accion sobre la que buscar')


    def mapper(self, key, line):
        data = line.split(',')

        accionNombre = str(self.options.accion)
        #print(accionNombre)
        
        accion = data[0]
        ultimoValor = data[1]
        maximo = data[5]
        minimo = data[6]
        momento = data[9]

        # Limpia los acentos
        accion = ''.join((c for c in unicodedata.normalize('NFD', accion) if unicodedata.category(c) != 'Mn'))

        if(accion==accionNombre):
            yield(accion, (float(ultimoValor), float(maximo), float(minimo), momento))
        
        

    def reducerCategory(self, accion, valores):
        minimoTotal = None
        maximoTotal = None
        ultimoValorFinal = None

        contador = 0
        for ultimoValor, maximo, minimo, momento in valores:
            if (contador==0): fechaInicial = momento
            ultimoValorFinal = ultimoValor
        
            if minimoTotal == None or minimo < minimoTotal: minimoTotal = minimo
            if maximoTotal == None or maximo > maximoTotal: maximoTotal = maximo
            contador = contador + 1

            if(maximoTotal>0):incremento = round(100-((ultimoValor * 100)/maximoTotal),2)
            if(minimoTotal>0):decrmento = round(100-((ultimoValor * 100)/minimoTotal),2)

        yield(accion, (minimoTotal, maximoTotal, decrmento, incremento))



    def steps(self):
        return [
            MRStep(mapper = self.mapper, reducer = self.reducerCategory),
        ]




#Executes class
if __name__ == '__main__':
    MRfirstJoin.run()
