from calendar import c
from ctypes import sizeof
from logging.config import dictConfig
from datetime import datetime

from mrjob.job import MRJob
from mrjob.step import MRStep


import collections
import unicodedata

class MRfirstJoin(MRJob):



    def mapper(self, key, line):
        data = line.split(',')

        if(len(data)>9):

            
            accion = data[0]
            ultimoValor = data[1]
            tipo= data[10]

            # Limpia los acentos
            accion = ''.join((c for c in unicodedata.normalize('NFD', accion) if unicodedata.category(c) != 'Mn'))
            yield(accion, (float(ultimoValor),tipo))
        
        

    def reducerCategory(self, accion, valores):
        
        acumulado = 0
        ultimaSubida = 0

        valoresSemanales = []
        valoresMensuales = []

        #yield(accion, list(valores))
        counter=0
        for valor, tipo in valores:
            if(tipo=="SEMANAL"):
                valoresSemanales.append(valor)
            valoresMensuales.append(valor)

        yield((accion, "SEMANAL", valoresSemanales), 1)
        yield((accion, "MENSUAL", valoresMensuales), 1)

    def reducerValues(self, accion_tipo_valores, _):
        accion = accion_tipo_valores[0]
        tipo = accion_tipo_valores[1]
        valores = accion_tipo_valores[2]

        acumulado = 0
        incrementoActual = 0
        acumuladoMensual = 0
        incrementoActualMensual = 0

        # Incremento semanal diario respecto al ultimo valor cotizado de cada sesion
        if tipo == "SEMANAL":
            counter = 0
            for valor in valores:
                if(len(valores)-counter > 1 ):
                    incrementoActual = valores[counter+1] - valor
                    acumulado = acumulado + incrementoActual

                counter = counter+1
            yield("SEMANAL", {accion: round(acumulado,3)})

        if tipo == "MENSUAL":
            counter = 0
            for valor in valores:
                if(len(valores)-counter > 1 ):
                    incrementoActualMensual = valores[counter+1] - valor
                    acumuladoMensual = acumuladoMensual + incrementoActualMensual

                counter = counter+1
            yield("MENSUAL", {accion: round(acumuladoMensual,3)})


    def reducerFinal(self, tipo, accion_valor):
        
        list_accion = list(accion_valor)

        dic_semanal = dict()
        dic_mensual = dict()

        if(tipo=="SEMANAL"): 
            for dato in list_accion:
                datoDict = dict(dato)
                for k,v in datoDict.items():
                    dic_semanal[k] = v

            dic_semanal = dict(sorted(dic_semanal.items(), key=lambda item: item[1], reverse=True))
            dic_semanal_acciones = list(dic_semanal.keys())
            dic_semanal_valores = list(dic_semanal.values())

            if(len(dic_semanal_acciones)>0):
                for i in range(5):  yield("Semanal "+str(i+1),(dic_semanal_acciones[i], dic_semanal_valores[i]))


        if(tipo=="MENSUAL"): 
            for dato in list_accion:
                datoDict = dict(dato)
                for k,v in datoDict.items():
                    dic_semanal[k] = v

            dic_mensual = dict(sorted(dic_mensual.items(), key=lambda item: item[1], reverse=True))
            dic_mensual_acciones = list(dic_mensual.keys())
            dic_mensual_valores = list(dic_mensual.values())

            if(len(dic_mensual_acciones)>0):
                for i in range(5): yield("Mensual "+str(i+1),(dic_mensual_acciones[i], dic_mensual_valores[i]))


    def steps(self):
        return [
            MRStep(mapper = self.mapper, reducer = self.reducerCategory),
            MRStep(reducer = self.reducerValues),
            MRStep(reducer = self.reducerFinal)
        ]




#Executes class
if __name__ == '__main__':
    MRfirstJoin.run()
