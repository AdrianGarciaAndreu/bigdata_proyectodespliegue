from calendar import c
from ctypes import sizeof
from logging.config import dictConfig
from datetime import datetime

from mrjob.job import MRJob
from mrjob.step import MRStep


import collections
import unicodedata

class MRfirstJoin(MRJob):

    def configure_args(self):
        super(MRfirstJoin, self).configure_args()
        self.add_passthru_arg('--accion', default='ACCIONA', help='Introduce la accion sobre la que buscar')


    def mapper(self, key, line):
        data = line.split(',')

        if(len(data)>9):
            accionNombre = str(self.options.accion)
            #print(accionNombre)
            
            accion = data[0]
            maximo = data[5]
            minimo = data[6]
            momento = data[9]
            tipo= data[10]
            hoy = int(data[11])

            # Limpia los acentos
            accion = ''.join((c for c in unicodedata.normalize('NFD', accion) if unicodedata.category(c) != 'Mn'))

            if(accion==accionNombre):
                yield(accion, (float(maximo), float(minimo), tipo, hoy))
        
        

    def reducerCategory(self, accion, valores):
        minimoUHora = None
        maximoUHora = None
        
        minimoUSemana = None
        maximoUSemana = None

        minimoUMes = None
        maximoUMes = None

        
        for maximo, minimo, tipo, hoy in valores:
            
        
            if tipo=="SEMANAL": # Los valores semanales se tienen en cuenta para el calculo semanal y mensual
                if minimoUMes == None or minimoUMes>minimo: minimoUMes = minimo
                if maximoUMes == None or maximoUMes<maximo: maximoUMes = maximo

                if minimoUSemana == None or minimoUSemana>minimo: minimoUSemana = minimo
                if maximoUSemana == None or maximoUSemana<maximo: maximoUSemana = maximo
                
                if hoy == 0: # minimos y máximos de la última hora con los datos del día de hoy
                    minimoUHora = minimo 
                    maximoUHora = maximo

            if tipo == "MENSUAL": # Se tienen en cuenta los datos mensuales solo para los maximos y minimos del mes
                if minimoUMes == None or minimoUMes>minimo: minimoUMes = minimo
                if maximoUMes == None or maximoUMes<maximo: maximoUMes = maximo
        


        if (minimoUHora!=None): yield((accion, "ultima HORA"), (minimoUHora, maximoUHora))
        yield((accion, "ultima SEMANA"), (minimoUSemana, maximoUSemana))
        yield((accion, "ultimo MES"), (minimoUMes, maximoUMes))



    def steps(self):
        return [
            MRStep(mapper = self.mapper, reducer = self.reducerCategory),
        ]




#Executes class
if __name__ == '__main__':
    MRfirstJoin.run()
