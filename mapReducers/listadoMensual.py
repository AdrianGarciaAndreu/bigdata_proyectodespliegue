from calendar import c
from ctypes import sizeof
from logging.config import dictConfig
from datetime import datetime

from mrjob.job import MRJob
from mrjob.step import MRStep


import collections
import unicodedata

class MRfirstJoin(MRJob):

    def mapper(self, key, line):
        data = line.split(',')

        accion = data[0]
        ultimoValor = data[1]
        maximo = data[5]
        minimo = data[6]
        momento = data[9]

        # Limpia los acentos
        accion = ''.join((c for c in unicodedata.normalize('NFD', accion) if unicodedata.category(c) != 'Mn'))


        yield(accion, (float(ultimoValor), float(maximo), float(minimo), momento))
        

    def reducerCategory(self, accion, valores):
        minimoS = None
        maximoS = None
        
        fechaInicial = None
        fechaFin = None
        
        contador = 0
        for ultimoValor, maximo, minimo, momento in valores:
            if (contador==0): fechaInicial = momento
            fechaFin = momento

            if minimoS == None or minimo < minimoS: minimoS = minimo
            if maximoS == None or maximo > maximoS: maximoS = maximo
            contador = contador + 1

        yield(accion, (fechaInicial, fechaFin, minimoS, maximoS))



    def steps(self):
        return [
            MRStep(mapper = self.mapper, reducer = self.reducerCategory),
        ]




#Executes class
if __name__ == '__main__':
    MRfirstJoin.run()
